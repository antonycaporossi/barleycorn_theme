<?php global $yith_wcwl, $woocommerce; ?>

<header id="masthead" class="site-header" role="banner">

    <?php if ( (isset($shopkeeper_theme_options['header_width'])) && ($shopkeeper_theme_options['header_width'] == "custom") ) : ?>
    <div class="row">		
        <div class="large-12 columns">
    <?php endif; ?>    
        
            <div class="site-header-wrapper" style="max-width:<?php echo esc_html($header_max_width_style); ?>">
                
                <div class="site-branding">
                    
                    <?php
					
                    if ( (isset($shopkeeper_theme_options['site_logo'])) && (trim($shopkeeper_theme_options['site_logo']) != "" ) ) {
						if (is_ssl()) {
                            $site_logo = str_replace("http://", "https://", $shopkeeper_theme_options['site_logo']);	
							if ($header_transparency_class == "transparent_header")	{
								if ( ($transparency_scheme == "transparency_light") && (isset($shopkeeper_theme_options['light_transparent_header_logo'])) && (trim($shopkeeper_theme_options['light_transparent_header_logo']) != "") ) {
									$site_logo = str_replace("http://", "https://", $shopkeeper_theme_options['light_transparent_header_logo']);	
								}
								if ( ($transparency_scheme == "transparency_dark") && (isset($shopkeeper_theme_options['dark_transparent_header_logo'])) && (trim($shopkeeper_theme_options['dark_transparent_header_logo']) != "") ) {
									$site_logo = str_replace("http://", "https://", $shopkeeper_theme_options['dark_transparent_header_logo']);	
								}
							}
                        } else {
                            $site_logo = $shopkeeper_theme_options['site_logo'];
							if ($header_transparency_class == "transparent_header")	{
								if ( ($transparency_scheme == "transparency_light") && (isset($shopkeeper_theme_options['light_transparent_header_logo'])) && (trim($shopkeeper_theme_options['light_transparent_header_logo']) != "") ) {
									$site_logo = $shopkeeper_theme_options['light_transparent_header_logo'];
								}
								if ( ($transparency_scheme == "transparency_dark") && (isset($shopkeeper_theme_options['dark_transparent_header_logo'])) && (trim($shopkeeper_theme_options['dark_transparent_header_logo']) != "") ) {
									$site_logo = $shopkeeper_theme_options['dark_transparent_header_logo'];
								}
							}
                        }
                        if ($header_transparency_class == "transparent_header transparency_dark") {
                            $site_logo = $shopkeeper_theme_options['dark_transparent_header_logo'];
                        }
						
						if ( (isset($shopkeeper_theme_options['sticky_header_logo'])) && (trim($shopkeeper_theme_options['sticky_header_logo']) != "" ) ) {
							if (is_ssl()) {
								$sticky_header_logo = str_replace("http://", "https://", $shopkeeper_theme_options['sticky_header_logo']);		
							} else {
								$sticky_header_logo = $shopkeeper_theme_options['sticky_header_logo'];
							}
						}
                        if(is_checkout() && empty( is_wc_endpoint_url('order-received') ) ){
                            $site_logo = $sticky_header_logo = get_template_directory_uri().'/images/barleycorn_logo_checkout.png';
                        }
						
                    ?>
    
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        	<img class="site-logo" src="<?php echo esc_url($site_logo); ?>" title="<?php bloginfo( 'description' ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
                            <?php if ( (isset($shopkeeper_theme_options['sticky_header_logo'])) && (trim($shopkeeper_theme_options['sticky_header_logo']) != "" ) ) { ?>
                            	<img class="sticky-logo" src="<?php echo esc_url($sticky_header_logo); ?>" title="<?php bloginfo( 'description' ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
                            <?php } ?>
                        </a>
                    
                    <?php } else { ?>
                    
                        <div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></div>
                    
                    <?php } ?>


                    <?php 
                    if(!is_checkout() || !empty( is_wc_endpoint_url('order-received') )){
                    ?>
                     <div class="site-tools <?php echo esc_html($site_tools_padding_class); ?>">
                        <ul>
                            
                            <?php if (class_exists('YITH_WCWL')) : ?>
                            <?php if ( (isset($shopkeeper_theme_options['main_header_wishlist'])) && ($shopkeeper_theme_options['main_header_wishlist'] == "1") ) : ?>
                            <li class="wishlist-button">
                                <a href="<?php echo esc_url($yith_wcwl->get_wishlist_url()); ?>" class="tools_button">
                                    <span class="tools_button_icon">
                                        <?php if ( (isset($shopkeeper_theme_options['main_header_wishlist_icon'])) && ($shopkeeper_theme_options['main_header_wishlist_icon'] != "") ) : ?>
                                        <img src="<?php echo esc_url($shopkeeper_theme_options['main_header_wishlist_icon']); ?>">
                                        <?php else : ?>
                                        <svg viewBox="0 0 36 32">
                                            <title>wishlist</title>
<path d="M23,3.7c1.8,0,3.4,0.7,4.6,2c1.2,1.3,1.9,3.1,1.9,5.1c0,2.1-0.8,3.9-2.5,6c-1.7,2-4.1,4.1-7,6.6c-1,0.9-2.1,1.8-3.3,2.9
    c-1.2-1-2.3-2-3.3-2.9l0,0c-2.9-2.4-5.3-4.6-7-6.6c-1.7-2.1-2.5-3.9-2.5-6c0-2,0.7-3.8,1.9-5.1c1.2-1.3,2.8-2,4.6-2
    c1.3,0,2.5,0.4,3.5,1.2c0.9,0.7,1.6,1.6,2,2.3c0.2,0.3,0.5,0.5,0.9,0.5s0.7-0.2,0.9-0.5c0.4-0.6,1-1.6,2-2.3
    C20.5,4.1,21.7,3.7,23,3.7 M23,0c-2.1,0-4.1,0.7-5.8,2c-0.2,0.2-0.4,0.3-0.6,0.5c-0.2-0.2-0.4-0.3-0.6-0.5c-1.7-1.3-3.7-2-5.8-2
    C7.3,0,4.7,1.1,2.8,3.2C1,5.2,0,7.9,0,10.8c0,3,1.1,5.6,3.3,8.4c1.9,2.3,4.5,4.5,7.5,7l0,0l0,0c1,0.8,2.1,1.8,3.3,2.8l2.4,2.1L19,29
    c1.2-1,2.3-2,3.3-2.8c3-2.6,5.6-4.8,7.5-7c2.3-2.8,3.3-5.4,3.3-8.4c0-2.9-1-5.6-2.8-7.6C28.4,1.1,25.8,0,23,0L23,0z"/>
                                        </svg>

                                        <?php endif; ?>
                                    </span>
                                    <?php
                                        count_topbar_antony('wishlist');
                                    ?>
                                </a>
                            </li>                           
                            <?php endif; ?>
                            <?php endif; ?>
                            
                            <?php if (class_exists('WooCommerce')) : ?>
                            <?php if ( (isset($shopkeeper_theme_options['main_header_shopping_bag'])) && ($shopkeeper_theme_options['main_header_shopping_bag'] == "1") ) : ?>
                            <?php if ( (isset($shopkeeper_theme_options['catalog_mode'])) && ($shopkeeper_theme_options['catalog_mode'] == 1) ) : ?>
                            <?php else : ?>
                            <?php
                                $class_fix = 'shopping-bag-button';
                                // ADD SIDE CART FUNCTIONALITY IF SHORTCODE EXISTS
                                if(shortcode_exists('xoo_wsc_cart')){
                                    $class_fix = 'xoo-wsc-cart-trigger';
                                }
                            ?>
                            <li class="<?php echo $class_fix;?>">
                                <a class="tools_button">
                                    <span class="tools_button_icon">
                                        <?php if ( (isset($shopkeeper_theme_options['main_header_shopping_bag_icon'])) && ($shopkeeper_theme_options['main_header_shopping_bag_icon'] != "") ) : ?>
                                        <img src="<?php echo esc_url($shopkeeper_theme_options['main_header_shopping_bag_icon']); ?>">
                                        <?php else : ?>
                                            <svg id="icon-bag" viewBox="0 0 27 32">
                                              <title>bag</title>
                                              <path d="M13.474 3.368c-1.86 0-3.368 1.508-3.368 3.368h6.737c0-1.86-1.508-3.368-3.368-3.368zM13.474 0c3.721 0 6.737 3.016 6.737 6.737l-1.94 2.954c-4.797 0-4.797 0-11.534 0v-2.954c0-3.721 3.016-6.737 6.737-6.737z"></path>
                                              <path d="M3.368 28.632h20.211v-18.526h-20.211v18.526zM0 6.737h26.947v25.263h-26.947v-25.263z"></path>
                                            </svg>
                                        <?php endif; ?>
                                    </span>
                                    <?php
                                        count_topbar_antony('cart');
                                    ?>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php endif; ?>
                            <?php endif; ?>

                            <?php if ( (isset($shopkeeper_theme_options['my_account_icon_state'])) && ($shopkeeper_theme_options['my_account_icon_state'] == "1") ) : ?>
                            <li class="my_account_icon">
                                <a class="tools_button" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
                                    <span class="tools_button_icon">
                                        <?php if ( (isset($shopkeeper_theme_options['custom_my_account_icon'])) && ($shopkeeper_theme_options['custom_my_account_icon'] != "") ) : ?>
                                        <img src="<?php echo esc_url($shopkeeper_theme_options['custom_my_account_icon']); ?>">
                                        <?php else : ?>
                                        <svg id="icon-account" viewBox="0 0 30 32">
                                          <title>account</title>
                                          <path d="M15.111 19.556c-5.4 0-9.778-4.378-9.778-9.778s4.378-9.778 9.778-9.778c5.4 0 9.778 4.378 9.778 9.778s-4.378 9.778-9.778 9.778zM15.111 16c3.436 0 6.222-2.786 6.222-6.222s-2.786-6.222-6.222-6.222c-3.436 0-6.222 2.786-6.222 6.222s2.786 6.222 6.222 6.222z"></path>
                                          <path d="M30.222 31.111c0-8.346-6.765-15.111-15.111-15.111s-15.111 6.765-15.111 15.111h3.556c0-6.382 5.174-11.556 11.556-11.556s11.556 5.174 11.556 11.556h3.556z"></path>
                                        </svg>
                                        <?php endif; ?>
                                    </span>
                                </a>
                            </li>
                            <?php endif; ?>

                            <?php if ( (isset($shopkeeper_theme_options['main_header_search_bar'])) && ($shopkeeper_theme_options['main_header_search_bar'] == "1") ) : ?>
                            <li class="offcanvas-menu-button search-button">
                                <a class="tools_button" data-toggle="offCanvasTop1">
                                    <span class="tools_button_icon">
                                        <?php if ( (isset($shopkeeper_theme_options['main_header_search_bar_icon'])) && ($shopkeeper_theme_options['main_header_search_bar_icon'] != "") ) : ?>
                                        <img src="<?php echo esc_url($shopkeeper_theme_options['main_header_search_bar_icon']); ?>">
                                        <?php else : ?>
                                        <i class="spk-icon spk-icon-search"></i>
                                        <?php endif; ?>
                                    </span>
                                </a>
                            </li>
                            <?php endif; ?>
                            
                        </ul>
                    </div>
                    <?php
                } else {
                    ?>
                        <div class="site-return_checkout"><a href="<?php echo wc_get_cart_url();?>" title="Ritorna allo shop">&larr; <span>Ritorna al carrello</span></a></div>
                    <?php
                }
                ?>
                </div><!-- .site-branding --> 

                <nav class="show-for-large main-navigation default-navigation" role="navigation">                    
                    <?php 
                        $walker = new rc_scm_walker;
						wp_nav_menu(array(
                            'theme_location'  => 'main-navigation',
                            'fallback_cb'     => false,
                            'container'       => true,
                            'items_wrap'      => '<ul class="%1$s">%3$s</ul>',
							'walker' 		  => $walker
                        ));
                    ?>           
                </nav><!-- .main-navigation -->                
                    
                <?php 

				$site_tools_padding_class = "";
				if ( (isset($shopkeeper_theme_options['main_header_off_canvas'])) && ($shopkeeper_theme_options['main_header_off_canvas'] == "1") ) {
					if ( (!isset($shopkeeper_theme_options['main_header_off_canvas_icon'])) || ($shopkeeper_theme_options['main_header_off_canvas_icon']) == "" ) {
                		$site_tools_padding_class = "offset";
					}
				}
				elseif ( (isset($shopkeeper_theme_options['main_header_search_bar'])) && ($shopkeeper_theme_options['main_header_search_bar'] == "1") ) {
                	if ( (!isset($shopkeeper_theme_options['main_header_search_bar_icon'])) || ($shopkeeper_theme_options['main_header_search_bar_icon']) == "" ) {
						$site_tools_padding_class = "offset";
					}
				}
                ?>
                <?php

                if(!is_checkout()){
                ?>
                <div class="site-tools <?php echo esc_html($site_tools_padding_class); ?>pleft">
                    <ul>
                        <li class="offcanvas-menu-button <?php if ( (isset($shopkeeper_theme_options['main_header_off_canvas'])) && ($shopkeeper_theme_options['main_header_off_canvas'] == "0") ) : ?>hide-for-xlarge<?php endif; ?>">
                            <a class="tools_button" data-toggle="offCanvasRight1">
                                <span class="tools_button_icon">
                                    <?php if ( (isset($shopkeeper_theme_options['main_header_off_canvas_icon'])) && ($shopkeeper_theme_options['main_header_off_canvas_icon'] != "") ) : ?>
                                    <img src="<?php echo esc_url($shopkeeper_theme_options['main_header_off_canvas_icon']); ?>">
                                    <?php else : ?>
                                    <div id="nav-icon3">
                                      <span></span>
                                      <span></span>
                                      <span></span>
                                      <span></span>
                                    </div>
                                    <?php endif; ?>
                                </span>
                                <div class="clear"></div>
                            </a>
                        </li>
                    </ul>
                </div>
                <?php
                }
                ?>      
            </div><!--.site-header-wrapper-->
        
    <?php if ( (isset($shopkeeper_theme_options['header_width'])) && ($shopkeeper_theme_options['header_width'] == "custom") ) : ?>
        </div><!-- .columns -->
    </div><!-- .row -->
    <?php endif; ?>

</header><!-- #masthead -->



<script>

	jQuery(document).ready(function($) {

    "use strict";

    var original_logo = $('.site-logo').attr('src');
	
		$(window).scroll(function() {
			
			if ($(window).scrollTop() > 0) {
				
				<?php if ( (isset($shopkeeper_theme_options['sticky_header'])) && (trim($shopkeeper_theme_options['sticky_header']) == "1" ) ) { ?>
					$('#site-top-bar').addClass("hidden");
					$('.site-header').addClass("sticky");
					<?php if ( (isset($shopkeeper_theme_options['sticky_header_logo'])) && (trim($shopkeeper_theme_options['sticky_header_logo']) != "" ) ) { ?>
						$('.site-logo').attr('src', '<?php echo esc_url($sticky_header_logo); ?>');
					<?php } ?>
				<?php } ?>
				
			} else {
				
				<?php if ( (isset($shopkeeper_theme_options['sticky_header'])) && (trim($shopkeeper_theme_options['sticky_header']) == "1" ) ) { ?>
					$('#site-top-bar').removeClass("hidden");
					$('.site-header').removeClass("sticky");
					<?php if ( (isset($shopkeeper_theme_options['sticky_header_logo'])) && (trim($shopkeeper_theme_options['sticky_header_logo']) != "" ) ) { ?>
						$('.site-logo').attr('src', original_logo);
					<?php } ?>
				<?php } ?>
				
			}
			
		});
	
	});
	
</script>


