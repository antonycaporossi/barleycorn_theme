jQuery(document).ready(function($){
	/*jQuery('.button:not(.disabled)').on('click', function(e){
		//e.preventDefault()
		//console.log($(this).text())
		save_text = $(this).text()
		$(this).html('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: transparent; display: block;" height="20px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" fill="none" stroke="#ffffff" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(359.848 49.9986 49.9986)"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform></circle></svg>')
	})*/
$('p#openChat, .openChat').click(function(e){
	e.preventDefault()
  $zopim.livechat.window.show();
})

$(".faq-anchor-wrapper a").on('click', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 50
    }, 500);
});


$('.cross-sells').fadeIn(1500)

	$('#newsletter_box #mc-embedded-subscribe-form').submit(function(e) {
	  var $this = $(this);
	  var button = $('#newsletter_box #subscribe').html()
	  $('#newsletter_box #subscribe').html('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: transparent; display: block;" height="20px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" fill="none" stroke="#ffffff" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(359.848 49.9986 49.9986)"><animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform></circle></svg>')
	  $.ajax({
		  type: "GET", // GET & url for json slightly different
		  url: "https://barleycorn.us3.list-manage.com/subscribe/post-json?u=5105a0791bc6a3b63e57d332b&amp;id=7079937b12&c=?",
		  data: $this.serialize(),
		  dataType    : 'json',
		  contentType: "application/json; charset=utf-8",
		  error       : function(err) { alert("Could not connect to the registration server."); },
		  success     : function(data) {
			  if (data.result != "success") {
			  	$('#newsletter_box .error_msg p').html(data.msg)
			  	$('#newsletter_box .error_msg p a').remove()
			  	$('#newsletter_box .error_msg').fadeIn('fast');
				  // Something went wrong, parse data.msg string and display message
			  } else {
			  	    $('#newsletter_box .nl_box').fadeOut('fast', function(){
				        $('#newsletter_box .success_msg').fadeIn('fast');
				    });
				  // It worked, so hide form and display thank-you message.
			  }
		  },
		  complete	: function(){
		  	$('#newsletter_box #subscribe').html(button)
		  }
	  });
	  return false;
	});
	$("#users_review").click(function() {
		$('html, body').animate({
			scrollTop: $("#reviews").offset().top
		}, 1000);
	});
	$('#custom_size_selector a:not(.noclick)').on('click', function(e){
		e.preventDefault()
		that = $(this)
		if(!that.hasClass("selected")){
			jQuery('#custom_size_selector a').removeClass('selected');
			that.addClass("selected")
		}
		var id = $(this).attr("id")
		//alert(id)
		$("select#pa_taglia option[value="+id+"]").prop('selected', true).trigger('change');
	})
	/*$("#footer > .row:nth-child(2) .title").on("click",function(){
		var t=$(this).closest(".col-sm-2")
		i=89+t.find("ul").outerHeight(!0);

		if (t.hasClass("is-open")){
			t.removeClass("is-open")
			t.height(69)
		} else {
			t.addClass("is-open")
			t.height(i)
		}
	} )*/
	if ($(window).width() <= 639) { 
		$("#site-footer .large-8 .widget-title").on("click",function(){
			console.log("")
			var t=$(this).closest("aside")
			i=89+20+t.find("ul").outerHeight(!0);

			if (t.hasClass("is-open") ) {
				t.removeClass("is-open")
				t.height(69)
			} else {
				t.addClass("is-open"),t.height(i)
			}
		} )
	}
	/*$('#offCanvasRight1').on('closed.zf.offcanvas', function(event) {
		jQuery('#custom_size_selector a').removeClass('selected');
	})*/
	/*$('#nav-icon3').on('click', function(){
		toggleNavIcon()
	});*/
	$('#offCanvasRight1').on('closed.zf.offcanvas opened.zf.offcanvas', function(){
		toggleNavIcon()
	});
	/*$('#offCanvasRight1').on('opened.zf.offcanvas', function(){
		toggleNavIcon()
	});*/

	function toggleNavIcon(){
		$("#nav-icon3").toggleClass('open');
	}
	jQuery('#show_filter_trigger').on('click', function(e){
		jQuery('#show_filter').slideToggle();
	})
	
	var account_tab_list = jQuery('.account-forms-container');
	
	account_tab_list.on('click','.account-tab-link',function(){
		
		if ( jQuery('.account-tab-link').hasClass('registration_disabled') ) {
			return false;
		} else {
		
			var that = jQuery(this),
				target = that.attr('href');
			console.log(target)
			
			jQuery('.account-tab-link').removeClass('current');
			that.addClass('current');
			
			jQuery('.account-forms').find(jQuery(target)).siblings().stop().fadeOut(function(){
				jQuery('.account-forms').find(jQuery(target)).fadeIn();
			});
			
			//$(target).siblings().stop().fadeOut(function(){
			//  $(target).fadeIn(); 
			//});
			
			return false;
		}
	});

		/**
		 * WooCommerce Bill to a Different Address
		 *
		 * @author Invisible Dragon
		 */
		var inputBoxTypes = "input, select, textarea";
		var shippingLength = "shipping_".length;

		// Add in hack to override val function to make it work properly with addons (such as saved addresses)
		var originalVal = jQuery.fn.val;
		jQuery.fn.val = function(){
			var result = originalVal.apply(this,arguments);
			if(arguments.length > 0 && jQuery(this).attr("id") && jQuery(this).attr("id").indexOf("shipping_") === 0) {
				jQuery(this).change(); // OR with custom event jQuery(this).trigger('value-changed');
			}
			return result;
		};

		// For every kind of input on the shipping side of things, try and
		// keep in sync with billing
		/*jQuery(inputBoxTypes, ".woocommerce-shipping-fields__field-wrapper").on("change keyup update", function () {
			if (!jQuery("#bill-to-different-address-checkbox").prop('checked')) {
				// Update the value on the billing side
				jQuery("#billing_" + jQuery(this).attr("id").substr(shippingLength)).val(jQuery(this).val());
			}
		}).trigger('change');*/
		// We trigger at the end in case of reloaded fields

		jQuery("#bill-to-different-address-checkbox").on("change", function () {
			var isChecked = jQuery(this).prop("checked");
			// Now go through every shipping field
			/*jQuery(inputBoxTypes, ".woocommerce-shipping-fields__field-wrapper").each(function () {
				var id = jQuery(this).attr("id").substr(shippingLength);
				console.log(id)
				// And if it has a billing counterpart, we can show/hide/reset the value
				if (isChecked) {
					jQuery("#billing_" + id + "_field").show(300);
					jQuery("#billing_company_field, #billing_pecsdi_field, #billing_piva_field").show(300);
				} else {
					jQuery("#billing_" + id + "_field").hide(300);
					jQuery("#billing_company_field, #billing_pecsdi_field, #billing_piva_field").hide(300);
					// We need to set the value to the shipping value
					jQuery("#billing_" + id).val(jQuery(this).val());
				}
			});*/
		if(isChecked){
			jQuery('.woocommerce-billing-fields__field-wrapper').show(300);
		} else {
			jQuery('.woocommerce-billing-fields__field-wrapper').hide();
		}
		}).trigger('change');

	// TOPBAR FADE
	var quotes = jQuery(".top_bar_part");
	var quoteIndex = -1;
	jQuery('.swiper-fix .swiper-container').each(function (index,value) {
		console.log(this);
	new Swiper (this, {
	  // Optional parameters
		slidesPerView: 5,
		spaceBetween: 15,
		initialSlide: 1,
		nextButton: jQuery(this).siblings('.swiper-button-next'),
		prevButton: jQuery(this).siblings('.swiper-button-prev'),
		breakpoints: {
			// when window width is <= 320px
			640: {
			  slidesPerView: 1,
			  spaceBetween: 0,
			},
			// when window width is <= 480px
			768:{
				slidesPerView: 3,
			  spaceBetween: 0,
			},
			1024: {
			  slidesPerView: 3,
			  spaceBetween: 0,
			},
			1200: {
			  slidesPerView: 4,
			  spaceBetween: 0,
			},
			2400:{
				slidesPerView: 5,
				spaceBetween: 15,
			}
		}
	})
	})

	function showNextQuote() {
		++quoteIndex;
		quotes.eq(quoteIndex % quotes.length)
			.fadeIn(2000)
			.delay(2000)
			.fadeOut(2000, showNextQuote);
	}
	
	showNextQuote();

	/*jQuery('#filters-offcanvas aside h3').click(function(){
		var pThis = jQuery(this).parent();
		if(pThis.hasClass('open')){
			pThis.toggleClass('open');
			 pThis.children('form').css('height','0');
			pThis.children('ul').css('height','0');
		} else {
			pThis.toggleClass('open');
			pThis.children('form').css('height','initial');
			 pThis.children('ul').css('height','initial');
		}

	})*/

	if (jQuery("body").hasClass("single-product")) {
		jQuery('#review_form_wrapper').hide();
		jQuery('ol.commentlist li').hide();
		jQuery('ol.commentlist li').slice(0, 3).show();
		jQuery(".more-reviews").click(function(){
			var showing = jQuery("ol.commentlist li:visible").length;
			var all = jQuery("ol.commentlist li").length;
			if(showing >= all){
				jQuery(".more-reviews").hide();
			}
			jQuery("ol.commentlist li").slice(showing - 1, showing + 2).show();
		});
		jQuery('.product_summary_middle .woocommerce-product-rating').click(function(){
			jQuery('#reviews')[0].scrollIntoView({ block: 'center',  behavior: 'smooth' })
		})
		jQuery('.show_reviews').click(function(){
			jQuery('#review_form_wrapper').fadeIn();
			jQuery('#review_form_wrapper')[0].scrollIntoView({ block: 'center',  behavior: 'smooth' })
		})
	}
});
/*

		var mySwiper = new Swiper ($(this), {
			
			// Optional parameters
			direction: 'horizontal',
			loop: true,
			grabCursor: true,
			preventClicks: true,
			preventClicksPropagation: true,
			autoplay: autoplay,
			speed: 600,
			effect: 'slide',
			
			// // If we need pagination
			pagination: $(this).find('.shortcode-slider-pagination'),
			paginationClickable: true,

			// // Navigation arrows
			nextButton: $(this).find('.swiper-button-next'),
			prevButton: $(this).find('.swiper-button-prev'),

			parallax: true,

			
			// // And if we need scrollbar
			// scrollbar: '.swiper-scrollbar',
		});*/