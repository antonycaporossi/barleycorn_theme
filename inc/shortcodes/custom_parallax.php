<?php

// [cparallax]
function custom_parallax_builder($params = array(), $content = null) {
	extract(shortcode_atts(array(
		'title' => 'Title',
		'description' => 'Subtitle',
		'cta_enable' => 'yes',
		'cta_text' => 'READ MORE',
		'cta_url'  => '',
		'parallax_min-height' => '490px',
		'parallax_text_align' => 'left',
		'parallax_text_position' => 'left',
		'bg_image_desktop' => '',
		'bg_image_mobile' => ''
	), $params));
	if (is_numeric($bg_image_desktop)) {
		$bg_image_desktop = wp_get_attachment_url($bg_image_desktop);
	}
	if (is_numeric($bg_image_mobile)) {
		$bg_image_mobile = wp_get_attachment_url($bg_image_mobile);
	}
	//$banner_with_img = '';
	$banner_simple_height = '<div class="cparallax cparallax--narrow">
<div class="cparallax-inner">
<div class="cparallax-bg rellax">
<img class="show-for-medium" src="'.$bg_image_desktop.'" alt="'.$title.'">
<img class="show-for-small-only" src="'.$bg_image_mobile.'" alt="'.$title.'">
</div>
<div class="cparallax_content align-'.$parallax_text_position.'">
<div class="cparallax-content-container">
<div class="cparallax-content-wrapper talign-'.$parallax_text_align.'">
<h5 class="title">'.$title.'</h5>
<p>'.$description.'</p>';
if($cta_enable == 'yes'){
	$link = vc_build_link($cta_url);
	$banner_simple_height .= '<a href="'.$link["url"].'" class="btn btn--white" title="'.$link["title"].'">'.$cta_text.'</a>';
}
$banner_simple_height .= '
</div>
</div>
</div>
</div>
</div>';
	/*if (is_numeric($bg_image)) {
		$bg_image = wp_get_attachment_url($bg_image);
		$banner_with_img = 'banner_with_img';
	}
	
	$content = do_shortcode($content);

	if ($new_tab == 'true')
	{
		$link_tab = 'onclick="window.open(\''.$link_url.'\', \'_blank\');"';
	}
	else 
	{
		$link_tab = 'onclick="location.href=\''.$link_url.'\';"';
	}
	
	$banner_simple_height = '
		<div class="shortcode_banner_simple_height '.$banner_with_img.'" '.$link_tab.'>
			<div class="shortcode_banner_simple_height_inner">
				<div class="shortcode_banner_simple_height_bkg" style="background-color:'.$bg_color.'; background-image:url('.$bg_image.')"></div>
			
				<div class="shortcode_banner_simple_height_inside" style="height:'.$height.'; border: '.$inner_stroke.' solid '.$inner_stroke_color.'">
					<div class="shortcode_banner_simple_height_content">
						<div><h3 style="color:'.$title_color.' !important">'.$title.'</h3></div>
						<div class="shortcode_banner_simple_height_sep" style="margin:'.$sep_padding.' auto; background-color:'.$sep_color.';"></div>
						<div><h4 style="color:'.$subtitle_color.' !important">'.$subtitle.'</h4></div>
					</div>
				</div>
			</div>';
	
	if ($with_bullet == 'yes') {
		$banner_simple_height .= '<div class="shortcode_banner_simple_height_bullet" style="background:'.$bullet_bg_color.'; color:'.$bullet_text_color.'"><span>'.$bullet_text.'</span></div>';
	}
	
	$banner_simple_height .= '</div>';*/
	
	return $banner_simple_height;
}

add_shortcode('cparallax', 'custom_parallax_builder');