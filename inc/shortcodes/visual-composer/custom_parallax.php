<?php

// [cparallax]

vc_map(array(
   "name"			=> "Custom Parallax",
   "category"		=> 'Content',
   "description"	=> "Custom Parallax",
   "base"			=> "cparallax",
   "class"			=> "",
   "icon"			=> "banner",

   
   "params" 	=> array(
      
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Title",
			"param_name"	=> "title",
		),
		
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Description",
			"param_name"	=> "description",
			"admin_label"	=> FALSE,
		),
		

		array(
			"type"			=> "checkbox",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Aggiungere CTA?",
			"param_name"	=> "cta_enable",
			"value"			=> array(
				"Yes"			=> 'yes',
			),
		),
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Testo CTA",
			"param_name"	=> "cta_text",
			"admin_label"	=> FALSE,
			"dependency" 	=> Array('element' => "cta_enable", 'value' => array('yes'))
		),
		array(
			"type"			=> "vc_link",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "CTA url",
			"param_name"	=> "cta_url",
			"dependency" 	=> Array('element' => "cta_enable", 'value' => array('yes'))
		),
		
		array(
			"type"			=> "textfield",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Parallax min-height",
			"param_name"	=> "parallax_min-height",
		),
 		array(
 			"type"			=> "dropdown",
 			"holder"		=> "div",
 			"class" 		=> "hide_in_vc_editor",
 			"admin_label" 	=> true,
 			"heading"		=> "Text Align",
 			"param_name"	=> "parallax_text_align",
 			"value"			=> array(
 				"Left"		=> "left",
 				"Right"		=> "right",
 				"Center"	=> "center"
 			),
 		),
 		array(
 			"type"			=> "dropdown",
 			"holder"		=> "div",
 			"class" 		=> "hide_in_vc_editor",
 			"admin_label" 	=> true,
 			"heading"		=> "Text Position",
 			"param_name"	=> "parallax_text_position",
 			"value"			=> array(
 				"Left"		=> "left",
 				"Right"		=> "right"
 			),
 		),
		array(
			"type"			=> "attach_image",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Background Image Desktop",
			"param_name"	=> "bg_image_desktop",
		),
		array(
			"type"			=> "attach_image",
			"holder"		=> "div",
			"class" 		=> "hide_in_vc_editor",
			"admin_label" 	=> true,
			"heading"		=> "Background Image Mobile",
			"param_name"	=> "bg_image_mobile",
		),
   )
   
));