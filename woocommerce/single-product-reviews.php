<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
if ( ! comments_open() ) {
	return;
}
if ( !have_comments() ){
	return;
}
?>

<div id="reviews" class="woocommerce-Reviews">
	<div id="comments" class="columns row large-centered small-12">
		<h2 class="woocommerce-Reviews-title text-center"><?php
			if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' && ( $count = $product->get_review_count() ) ) {
				echo 'Le recensioni dei clienti';
			} else {
				_e( 'Reviews', 'woocommerce' );
			}
		?></h2>

		<?php if ( have_comments() ) : ?>
			<div class="row" style="margin: 0 auto">
				<div class="columns large-7 small-12 text-center small-collapse fix_width_score">
					<div class="left_rating columns small-5">
						<div class="rating_score">
							<?php
							woocommerce_template_single_rating_reviews_antony();
							?>
						</div>
					</div>
					<div class="right_rating columns small-7">
						<ul class="no-bullet">
							<?php
							foreach(get_review_count($product->get_rating_counts()) as $key=>$count_r){
								$percent = 0;
								if($count_r > 0){
									$percent = ($count_r/$count)*100;
								}
								?>
								<li><p><?php echo $key;?></p><div class="rating_bar"><div class="rating_bar_value" style="width: <?php echo $percent;?>%">&nbsp;</div></div></li>
								<?php
							}
							?>
						</ul>
					</div>
				</div>
				<div class="columns large-4 end small-12 content_review_description">
					<p class="button show_reviews">SCRIVI UNA RECENSIONE</a>
					<p class="review_description show-for-large margin-top">Cras molestie convallis arcu eget finibus. Donec feugiat auctor mauris, rutrum pretium arcu vehicula malesuada. Nullam venenatis nec arcu eget varius.</p>
				</div>
			</div>
			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>
<?php
				if($count > 0){
					?>
					<p class="more-reviews button" style="background: #000 !important">Vedi altre recensioni</p>
					<?php
				}

?>
				<p class="show_reviews button" style="">Lascia una recensione</p>
			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
					'prev_text' => '&larr;',
					'next_text' => '&rarr;',
					'type'      => 'list',
				) ) );
				echo '</nav>';
			endif; ?>

		<?php else : ?>

			<p class="woocommerce-noreviews"><?php _e( 'There are no reviews yet.', 'woocommerce' ); ?></p>

		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>

		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
					$commenter = wp_get_current_commenter();
					$comment_form = array(
						'title_reply'          => have_comments() ? sprintf(__( 'Lascia una recensione per %s', 'woocommerce' ), get_the_title() ) : sprintf( __( 'Be the first to review &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
						'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
						'title_reply_before'   => '<span id="reply-title" class="comment-reply-title">',
						'title_reply_after'    => '</span><p class="review_form_subtitle">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',
						'comment_notes_before'			=> '',
						'comment_notes'  => '',
						'fields'               => array(
							'author' => '<p class="comment-form-author">' . '<label for="author">' . esc_html__( 'Name', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label> ' .
										'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></p>',
							'email'  => '<p class="comment-form-email"><label for="email">' . esc_html__( 'Email', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label> ' .
										'<input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></p>',
						),
						'label_submit'  => __( 'Submit', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => '',
					);

					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a review.', 'woocommerce' ), esc_url( $account_page_url ) ) . '</p>';
					}

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__( 'Your rating', 'woocommerce' ) . '</label><select name="rating" id="rating" aria-required="true" required>
							<option value="">' . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
							<option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
							<option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
							<option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
							<option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
							<option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
						</select></div>';
					}

					$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . esc_html__( 'Your review', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" required></textarea></p>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>

	<?php else : ?>

		<p class="woocommerce-verification-required"><?php _e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>
<?php
		 if ( $product->get_upsell_ids() ) : ?>
			<div class="single_product_summary_upsell">
						<?php do_action( 'woocommerce_after_single_product_summary_upsell_display' ); ?>
			</div><!-- .single_product_summary_upsell -->
		<?php endif; ?>
