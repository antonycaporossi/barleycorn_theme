<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
 
global $shopkeeper_theme_options;
?>

<div class="row <?php if ( !is_archive() ) :?> swiper-fix<?php endif; ?>">
	<?php if ( !is_archive()) :?>
	    <div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div>
	<?php endif;?>
	<?php
	$base_link = $_SERVER['REQUEST_URI'];
$_chosen_attributes = WC_Query::get_layered_nav_chosen_attributes();


			if ( ! empty( $_chosen_attributes ) ) {
				echo '<div class="widget_layered_nav_filters widget"><ul>';
				echo '<h3>Filtri attivi</h3>';
				foreach ( $_chosen_attributes as $taxonomy => $data ) {
					foreach ( $data['terms'] as $term_slug ) {
						$term = get_term_by( 'slug', $term_slug, $taxonomy );
						if ( ! $term ) {
							continue;
						}

						$filter_name    = 'filter_' . sanitize_title( str_replace( 'pa_', '', $taxonomy ) );
						$current_filter = isset( $_GET[ $filter_name ] ) ? explode( ',', wc_clean( wp_unslash( $_GET[ $filter_name ] ) ) ) : array(); // WPCS: input var ok, CSRF ok.
						$current_filter = array_map( 'sanitize_title', $current_filter );
						$new_filter     = array_diff( $current_filter, array( $term_slug ) );

						$link = remove_query_arg( $filter_name, $base_link);

						if ( count( $new_filter ) > 0 ) {
							$link = add_query_arg( $filter_name, implode( ',', $new_filter ), $link );
						}

						echo '<li class="chosen"><a rel="nofollow" aria-label="' . esc_attr__( 'Remove filter', 'woocommerce' ) . '" href="' . esc_url( $link ) . '">' . esc_html( $term->name ) . '</a></li>';
					}
				}
				echo "</ul></div>";
			}
			
	?>
	<div class="large-12 columns <?php if ( !is_archive() ) :?> swiper-container<?php endif; ?>">
		<ul id="products-grid" class="row products products-grid <?php shopkeeper_loop_columns_class(); if ( !is_archive() ) :?> swiper-wrapper<?php endif;?>">