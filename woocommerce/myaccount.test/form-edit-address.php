<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;
wc_get_template( 'myaccount/template_antony/myaccount_header.php' );
$page_title = ( 'billing' === $load_address ) ? __( 'Billing address', 'woocommerce' ) : __( 'Shipping address', 'woocommerce' );


do_action( 'woocommerce_before_edit_account_address_form' ); ?>
<style>
.woocommerce-edit-address .b3grid form, .woocommerce-edit-address .b3grid .my_address_container{
    max-width: 768px;
    margin: 0 auto;
}
.woocommerce-edit-address #billing_state_field .woocommerce-input-wrapper,
.woocommerce-edit-address #billing_country_field .woocommerce-input-wrapper,
.woocommerce-edit-address #shipping_state_field .woocommerce-input-wrapper,
.woocommerce-edit-address #shipping_country_field .woocommerce-input-wrapper
{
    width: 100%;
}
.woocommerce-edit-address .woocommerce-input-wrapper .select2-selection{
    border-color: #e4e4e4;
    border-radius: 0;
}
.woocommerce-edit-address form .form-row input.input-text{
    border-color: #e4e4e4;
    background: #FFF;
}
.woocommerce-edit-address form .form-row label{
    line-height: 1em;
    margin-top: 17px;
    margin-bottom: 2px;
}
</style>
<?php if ( ! $load_address ) : ?>
    <div class="my_address_container">
	   <?php wc_get_template( 'myaccount/my-address.php' ); ?>
    </div>
<?php else : ?>

	<form method="post">

		<h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title, $load_address ); ?></h3><?php // @codingStandardsIgnoreLine ?>

		<div class="woocommerce-address-fields">
			<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

			<div class="woocommerce-address-fields__field-wrapper">
				<?php
				foreach ( $address as $key => $field ) {
					woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
				}
				?>
                <div class="clear"></div>
			</div>

			<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>
            <div class="clear"></div>
			<p>
				<button type="submit" class="button antony_button" name="save_address" value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>"><?php esc_html_e( 'Save address', 'woocommerce' ); ?></button>
				<?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>

				<input type="hidden" name="action" value="edit_address" />
			</p>
		</div>

	</form>

<?php endif; ?>

<?php do_action( 'woocommerce_after_edit_account_address_form' );
wc_get_template( 'myaccount/template_antony/myaccount_footer.php' );
?>
