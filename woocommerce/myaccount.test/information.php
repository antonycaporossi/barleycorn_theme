<?php
/**
 * Richiedi reso
 *
 *
 * @package Barleycorn v3
 * @version 1.0.0
 * @author Antony Caporossi 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
//$my_account_order_number_antony = 'Ordine '. _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number();
add_filter('the_title', function(){
    return 'Richiedi un cambio/reso';
});
$working_days = getWorkingDays();
print_r($working_days);
$return = '';
foreach( $working_days as $wd){
    $return .= '<option value="'.$wd.'">'.$wd.'</option>';
}
?>

<?php
wc_get_template( 'myaccount/template_antony/myaccount_header.php');
?>
<style>
.return_request_container{
	width: 100%;
	max-width: 700px;
    margin: auto;
    text-align: left;
    -webkit-box-shadow: 0 23px 30px 0 rgba(0,0,0,0.13);
    box-shadow: 0 23px 30px 0 rgba(0,0,0,0.13);
    background-color: white;
    clear: both;
    display: flow-root;
    padding: 30px;
    margin-top: 20px;
    background-color: #f1f1f1;
}
.return_request_container .form-control {
    display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555;
    vertical-align: middle;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 1px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
.return_request_container label.control-label{
	width: 20%;
	    display: inline-block;
    margin-bottom: 5px;
    font-weight: 700;
}
.return_request_container .input-form{
	display: inline-block;
	width: 60%;
}
.submit_container{
	text-align: center;
width: 100%;
    padding: 20px;
}
.submit_container > input{
	    border: none;
    border-radius: 4px;
    line-height: 30px;
    font-size: 21px;
    font-weight: 700;
    padding: 10px 30px;
    -webkit-box-shadow: 0 23px 30px 0 rgba(0,0,0,0.13);
    box-shadow: 0 23px 30px 0 rgba(0,0,0,0.13);
    display: inline-block;
    margin: 0 auto;
}
.radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}
.radio-inline input[type=radio] {
    position: absolute;
    margin-top: 4px\9;
    margin-left: -20px;
}
.radio-inline+.radio-inline {
    margin-top: 0;
    margin-left: 10px;
}
.giorno_ritiro, .returnType_radio{
	display: none;
}
.warning{
	background: #e0f7e1;
    padding: 10px 20px;
    border: 1px solid #8BC34A;
    color: #4CAF50;
    font-weight: bold;
    margin-top: 15px;
}
.woocommerce-account .page-content .success_message h2{
	margin-top: 10px;
	border-bottom-color: #4CAF4F;
}
.message.already_done {
    background: #ffe5dd;
    border: 1px solid #d6a4b5;
    padding: 10px 20px;
    color: #863015;
    display: none;
}
</style>
<div class="col-xs-12">
	<div class="return_request_container">
		<form method="post" id="returnRequest">
			<p style="margin-bottom: 10px;">
				Verrai ricontattato dal Customer Service il prima possibile tramite email o telefonicamente.<br/>
						Morbi ligula urna, porttitor a finibus eget, euismod sit amet arcu.<br/>
			</p>
			<div class="form-inputs">
				<div class="form-group">
					<label for="returnEmail" class="control-label">Email</label>
					<div class="input-form">
						<input type="email" class="form-control" name="email" id="returnEmail" placeholder="Email" required>
					</div>
				</div>
				<div class="form-group">
					<label for="returnONumber" class="control-label">Numero d'ordine</label>
					<div class="input-form">
						<input type="number" class="form-control" name="order_number" id="returnONumber" placeholder="Numero d'ordine" required>
					</div>
				</div>
				<div class="form-group returnType_radio">
					<label for="returnONumber" class="control-label">Cosa vuoi fare?</label>
					<div class="input-form" style="text-align:center">
						<label class="radio-inline">
					      <input type="radio" name="returnType" value="change" disabled required>Voglio fare il cambio
					    </label>
					    <label class="radio-inline">
					      <input type="radio" name="returnType" value="return"  disabled required>Voglio fare il reso
					    </label>
                    </div>
				</div>
				<!--<div class="form-group giorno_ritiro">
					<label for="request_ritiro" class="control-label">Giorno ritiro:</label>
					<div class="input-form">
						<select id="request_ritiro" disabled class="form-control" name="request_ritiro" required>
							<option selected value="">Seleziona il giorno per il ritiro</option>
							<?php echo $return;?>
						</select>
                    </div>
				</div>-->
				<div class="message already_done"><strong>La tua richiesta è stata già presa in carico.</strong><br/>Attendi la conferma del nostro customer care entro 24/48 ore.</div>
			</div>
			<div class="submit_container">
				<input type="submit" value="CONFERMA" />
			</div>
		</form>
		<div class="success_message" >
				<div class="warning" >
					<h2>Abbiamo ricevuto la tua richiesta</h2>
					<p>
						Ciao, la tua richiesta è stata inoltrata correttamente.<br>
						Il nostro Customer Care provvederà a prenotare il ritiro per te. Nel frattempo prepara i prodotti da restituire.
					</p>
					<ul>
	<li><span style="font-size:14px">Inserisci le calzature nella loro scatola originaria Barleycorn e inserisci la stessa nell’imballo in cartone bianco con cui ti è stata spedita.</span></li>
	<li><span style="font-size:14px">Chiudi,&nbsp;sigilla con cura e applica all’esterno del collo l’etichetta bianca&nbsp;(coprendo l’etichetta precedente) che si trova insieme al prodotto.</span></li>
</ul>
				</div>
		</div>
	</div>
</div>
<?php
/*if(is_user_logged_in()){
	$current_user = wp_get_current_user();
	echo "ID: ".$current_user->ID."<br/>";
	echo "Email: ".$current_user->user_email."<br/>";


	if($numorders > 0){

	} else {

	}


} else */

$numorders = wc_get_customer_order_count( $current_user->ID );

wc_get_template( 'myaccount/template_antony/myaccount_footer.php');
?>