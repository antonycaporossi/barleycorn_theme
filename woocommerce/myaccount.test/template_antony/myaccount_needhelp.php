<?php
/**
 * Need Help Template
 *
 * @author 		Antony Caporossi
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>		</div></div></div></div>
<div class="b3grid">
	<div class="needhelp_container">
		<div class="container">
			<div class="row needhelp">
				<div class="col-xs-12 col-sm-12 text-center">
					<h2>Hai bisogno di aiuto?</h2>
				</div>
				<div class="col-xs-4 col-sm-4 text-center">
					<a href="#" id="open-cchat">
						<i class="fa fa-comment-o"></i>
						<p>Chat</p>
					</a>
					<small>Dal lunedì al venerdì dalle 9 alle 18</small>
				</div>
				<div class="col-xs-4 col-sm-4 text-center">
					<a href="tel:07331970841">
						<i class="fa fa-phone"></i>
						<p>Al 0733 1970841</p>
					</a>
					<small>Dal lunedì al venerdì dalle 9 alle 18</small>
				</div>
				<div class="col-xs-4 col-sm-4 text-center" >
					<a href="mailto:infobarleycorn@fashionm.it">
						<i class="fa fa-envelope-o"></i>
						<p>Email</p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>