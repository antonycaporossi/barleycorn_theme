<?php
/**
 * My Account page
 *
 * @author 		Antony Caporossi
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

<?php

global $woocommerce, $yith_wcwl, $order, $wp;
$request = explode( '/', $wp->request );
if ( version_compare( WOOCOMMERCE_VERSION, "2.1.0" ) >= 0 ) {
	wc_print_notices();
} else {
	$woocommerce->show_messages();
}

?>
<section class="b3grid">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 ">
				<div class="top_menu_myaccount">
					<ul class="hidden-xs">
						<?php top_menu_myaccount($request); ?>
					</ul>
					<?php
					if( ( end($request) !== 'account' ) ){ 
						?>
					<ul class="visible-xs hidden-sm">
						<li><a onclick="history.back();">< Torna indietro</a></li>
					</ul>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title">
					<?php 
						if ( is_wc_endpoint_url( 'view-order' ) ) {
							echo $my_account_order_number_antony;
						} else {
							the_title();
						}
						
					?>
				</h1>
			</div>