<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
				<style>

				</style>
<?php
add_filter('the_title', 'new_title', 10, 2);
function new_title($title) {
    return 'I tuoi ordini';
}
wc_get_template( 'myaccount/template_antony/myaccount_header.php' );
do_action( 'woocommerce_before_account_orders', $has_orders ); ?>

<?php if ( $has_orders ) :
		foreach ( $customer_orders->orders as $customer_order ) :
		$order      = wc_get_order( $customer_order );
		$item_count = $order->get_item_count();
		$items 		= $order->get_items();

				?>
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-12 col-sm-4" style="background: #f4f4f4">
				<div class="order_infos">
					<h6>Ordine N. <strong><?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?></strong></h6>
					<p>Data Ordine: <strong><?php echo esc_html( wc_format_datetime( $order->get_date_created(), 'd/m/Y' ) ); ?></strong><br/>
						<strong>Indirizzo di consegna</strong><br/>
						<?php
							if($order->get_billing_company()){
								echo $order->get_billing_company().'<br/>';
							}
						?>
						<?php echo $order->get_billing_first_name().' '.$order->get_billing_last_name()?>
					</p>
					<a href="/account/view-order/<?php echo $order->get_order_number();?>/" class="btn">DETTAGLI</a>
					<?php echo fix_return_button_account_orders();?>
				</div>
			</div>
			<?php
			?>
			<div class="col-xs-12 col-sm-8">
				<table class="woocommerce-orders-table shop_table_antony woocommerce-MyAccount-orders my_account_orders account-orders-table table-responsive" border="1" bordercolor="#e5e5e5" cellpadding="5">
					<thead>
						<tr>
							<th colspan="4" class="<?php echo $order->get_status();?>">
								<strong class="pull-left">Stato Ordine:
									<?php 
										echo esc_html( wc_get_order_status_name( $order->get_status() ) ).'</strong>';
										echo '<span class="pull-right"><strong>'.tracking_myaccount($order).'</strong></span>';
									?>
							</th>
						</tr>
						<tr>
							<th colspan="2"><strong>Prodotti</strong></th>
							<th><strong>Quantità</strong></th>
							<th><strong>Amount</strong></th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach($items as $item_key => $item):
								
							?>
							<tr>
								<td class="ma_product_image" data-title="Prodotto" style="text-align: center; background: whitesmoke;">
									<?php
									//print_r($item);
									//echo $item->get_product_id();
									$image_size = apply_filters( 'single_product_archive_thumbnail_size', 'thumbnail' );
 									echo '<img src="'.get_the_post_thumbnail_url( $item->get_product_id(), $image_size ).'"  style="width: 110px"/>';
									?>
								</td>
								<td class="ma_product_title">
									<?php
										$product_name = explode(' - ', $item->get_name());
										echo '<strong>'.$product_name[0].'</strong>';
										if($product_name[1]){
											echo '<br/><small>- Misura '.$product_name[1].'</small>';
										}
									?>
								</td>
								<td class="ma_product_quantity">
									<?php echo $item->get_quantity();?>
								</td>
								<td class="ma_product_amount">
									<?php
										if($item->get_subtotal() === $item->get_total()){
											echo wc_price($item->get_total());
										} else {
											echo wc_price($item->get_total());
										}
									?>
									
								</td>
							</tr>
							<?php
							endforeach;
							?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4" style="text-align: right">
								<a href="/account/view-order/<?php echo $order->get_order_number();?>/" class="btn">DETTAGLI</a>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	<?php endforeach; ?>









	<?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

	<?php if ( 1 < $customer_orders->max_num_pages ) : ?>
		<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
			<?php if ( 1 !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) ); ?>"><?php _e( 'Previous', 'woocommerce' ); ?></a>
			<?php endif; ?>

			<?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) ); ?>"><?php _e( 'Next', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>

<?php else : ?>
	<div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php _e( 'Go shop', 'woocommerce' ) ?>
		</a>
		<?php _e( 'No order has been made yet.', 'woocommerce' ); ?>
	</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_account_orders', $has_orders );
wc_get_template( 'myaccount/template_antony/myaccount_footer.php' );
?>
