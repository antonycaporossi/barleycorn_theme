<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
require_once(ABSPATH.'/script/parser/simple_html_dom.php');
$my_account_order_number_antony = 'Ordine '. _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number();
wc_get_template( 'myaccount/template_antony/myaccount_header.php', array('my_account_order_number_antony'=>$my_account_order_number_antony));
?>
				<style>
					.meter{
						height: 20px;
						background: #ececec;
					}
					.green > span{
						background-color: #5cb85c;
					}
					.meter > span{
						height: 100%;
						display: block;
						width: 0%;
					}
					.legends-bar{
						padding: 10px 0;
					}
					.legends-bar div{
						position: relative;
						padding-top: 4px;
					}
					.stato_1{
						width: 20%;
						position: relative;
						float: left;
						text-align: left;
					}
					.stato_2{
						width: 30%;
						position: relative;
						float: left;
						text-align: center;
					}
					.stato_2 .triangle{
						left: 50%;
						margin-left: -10px;
					}
					.triangle{
						position: absolute;
						width: 0;
						height: 0;
						border-style: solid;
						border-width: 0 10px 10px 10px;
						border-color: transparent transparent #6e715a transparent;
						top: -7px;
						left: 3px;
					}
					.stato_3{
						width: 40%;
						position: relative;
						float: right;
						text-align: right;
					}
					.stato_3 .triangle{
						left: inherit;
						right: 3px;
					}
					.tracking_module table tr:nth-child(even) {
					  background-color: #f2f2f2;
					}
					.tracking_module table thead th {
					  background-color: #dadada;
					  padding: 5px;
					  text-align: left;
					}
					.tracking_module table{
						margin: 20px 0;
					}
					.tracking_module table td{
						padding: 10px;
					}

				</style>
	<div class="col-xs-12">
		<div class="row">

			<?php

			?>
			<div class="col-xs-12 col-sm-12 col-md-8  pull-right">
				<div class="tracking_module">

				<?php
				
				$html = file_get_html('https://vas.brt.it/vas/sped_det_show.hsm?referer=sped_numspe_par.htm&ChiSono=052090010140289&ClienteMittente=&DataInizio=&DataFine=&RicercaChiSono=Ricerca');
				$table_stato_dati = $html->find('#box_contenuti .table_stato_dati');
				if($table_stato_dati){

					$consegnata = 0;
					$width = '';
					$table = '
					<table border="0" width="100%">
						<thead>
							<tr>
								<th>Data</th>
								<th>Ora</th>
								<th>Filiale</th>
								<th>Evento</th>
							</tr>
						</thead>
					';
					foreach($table_stato_dati as $element)
						$body = $element->find('tr');
						$table .= '<tbody>';
				    	foreach($body as $element3){
				    		$table .= '<tr>';
				    		foreach($element3->find('td') as $element4){
				    			$table .= '<td>'.$element4->innertext.'</td>';
				    			if($element4->innertext == 'DATI SPEDIZ. TRASMESSI A BRT'){
				    				$consegnata++;
				    			}
				    			if($element4->innertext == 'CONSEGNATA'){
				    				$consegnata++;
				    			}
				    		}
				    		$table .= '</tr>';
				    	}
				    	?>
			    		</tbody>
					</table>
				    	<?php
				    	$table .= '</tbody></table>';
				    	$width = 10;
				    	if($consegnata > 0){
				    		$width = 50*$consegnata;
				    	}
?>
					<div class="meter green">
						<span class="status-bar" data-tracking="<?php echo $width;?>" > </span>
					</div>
					<div class="legends-bar">
						<div class="stato_1"><span class="triangle"></span>Ordinato</div>
						<div class="stato_2"><span class="triangle"></span>Spedito</div>
						<div class="stato_3"><span class="triangle"></span>Consegnato</div>
						<div class="clearfix"></div>
					</div>
<?php

			    	echo $table;
			    	?>
			    	<script>
			    	setTimeout(function(){
			    		$('.status-bar').animate({'width': $('.status-bar').data('tracking')+'%'}, 2500);
			    	 }, 1500);
			    	</script>
			    	<?php
			    }
			    	?>
				</div>
				<table class="woocommerce-orders-table shop_table_antony woocommerce-MyAccount-orders my_account_orders account-orders-table table-responsive" border="1" bordercolor="#e5e5e5" cellpadding="5">
					<thead>
						<tr>
							<th colspan="4" class="<?php echo $order->get_status();?>">
								<strong class="pull-left">Stato Ordine:
									<?php 
										echo esc_html( wc_get_order_status_name( $order->get_status() ) ).'</strong>';
										echo '<span class="pull-right"><strong>'.tracking_myaccount($order).'</strong></span>';
									?>
							</th>
						</tr>
						<tr>
							<th colspan="2"><strong>Prodotti</strong></th>
							<th><strong>Quantità</strong></th>
							<th><strong>Amount</strong></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$items 		= $order->get_items();
							foreach($items as $item_key => $item):
								
							?>
							<tr>
								<td class="ma_product_image" data-title="Prodotto" style="text-align: center; background: whitesmoke;">
									<?php
									//print_r($item);
									//echo $item->get_product_id();
									$image_size = apply_filters( 'single_product_archive_thumbnail_size', 'thumbnail' );
 									echo '<img src="'.get_the_post_thumbnail_url( $item->get_product_id(), $image_size ).'"  style="width: 110px"/>';
									?>
								</td>
								<td class="ma_product_title" data-title="Nome prodotto">
									<?php
										$product_name = explode(' - ', $item->get_name());
										echo '<strong>'.$product_name[0].'</strong>';
										if($product_name[1]){
											echo '<br/><small>- Misura '.$product_name[1].'</small>';
										}
									?>
								</td>
								<td class="ma_product_quantity" data-title="Quantità">
									<?php echo $item->get_quantity();?>
								</td>
								<td class="ma_product_amount" data-title="Amount">
									<?php
										if($item->get_subtotal() === $item->get_total()){
											echo wc_price($item->get_total());
										} else {
											echo wc_price($item->get_total());
										}
									?>
									
								</td>
							</tr>
							<?php
							endforeach;
							?>
					</tbody>
					<tfoot>
						<?php
							foreach ( $order->get_order_item_totals() as $key => $total ) {
								if($key !== 'payment_method'){
								?>
								<tr>
									<th scope="row" colspan="2"><?php echo $total['label']; ?></th>
									<td colspan="2" data-title="<?php echo $total['label']; ?>"><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : $total['value']; ?></td>
								</tr>
								<?php
								}
							}
						?>
					</tfoot>
				</table>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="order_infos">
					<a href="/account/orders/" class="btn">< INDIETRO</a>
					<?php echo fix_return_button_account_orders();?>
					<h6>Ordine N. <strong><?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?></strong></h6>
					<p>Data Ordine: <strong><?php echo esc_html( wc_format_datetime( $order->get_date_created(), 'd/m/Y' ) ); ?></strong><br/>
					</p>
					<div class="view-order-address billing_address">
						<strong>Indirizzo di fatturazione</strong>
						<p>
							<?php echo formatted_address($order, 'billing');?>
						</p>
					</div>
					<?php
						if($order->customer_message){
							?>
								<div class="view-order-address customer_notes">
									<strong>Note per la consegna</strong>
									<p>
										<?php echo $order->customer_message;?>
									</p>
								</div>
							<?php
						}
					?>

					<div class="view-order-address shipping_address">
						<strong>Indirizzo di consegna</strong>
						<p>
							<?php echo formatted_address($order, 'shipping');?>
						</p>
					</div>
					<div class="view-order-address payment_method">
						<strong>Metodo di pagamento</strong>
						<p>
							<?php echo $order->get_payment_method_title();?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
wc_get_template( 'myaccount/template_antony/myaccount_footer.php' );
?>
