<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
wc_get_template( 'myaccount/template_antony/myaccount_header.php' );
$current_user = wp_get_current_user();
?>
<style>
#welcome_box_myaccount{
	text-align: center;
	padding: 20px 0;
	min-height: 600px;
    background: url(/wp-content/themes/barleycorn/images/welcome_image.jpg) #efefef;
    background-position: 0 190px;
    background-repeat: no-repeat;
    background-size: cover;
}
#welcome_box_myaccount:after {
    background: #efefef;
    display: block;
    width: 100%;
    height: 100%;
    position: absolute;
    content: "";
    top: 0;
    opacity: 0.8;
}
#welcome_box_myaccount h4{
	font-size: 3em;
}
#welcome_box_myaccount p a{
	margin: 0 auto;
    float: none;
    display: inline-block;
    border: none;
    border-radius: 4px;
    line-height: 30px;
    font-size: 21px;
    font-weight: 700;
    padding: 10px 30px;
    -webkit-box-shadow: 0 23px 30px 0 rgba(0,0,0,0.13);
    box-shadow: 0 23px 30px 0 rgba(0,0,0,0.13);
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
    margin-top: 20px;
}
#welcome_box_myaccount .welcome_box_myaccount_cta{
	position: relative;
    top: 50%;
    transform: translateY(50%);
    z-index: 100;
}
.myaccount_menu{
	float: left;
}
.myaccount_menu ul li a {
    font-weight: bold;
    color: #000000;
}
.woocommerce-account .myaccount_icon {
    width: 140px;
    padding: 17px 25px;
    float: left;
}

</style>

			<div class="col-xs-12 col-sm-6" id="welcome_box_myaccount">
				<div class="welcome_box_myaccount_cta">
					<h4>Ciao, <strong><?php echo $current_user->first_name;?></strong></h4>
					<p>Ricomincia ora il tuo shopping!<br/>
						<a href="/shop/">VAI ALLO SHOP</a>
					</p>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<div class="myaccount_section ordini">
					<div class="myaccount_icon ordini">
						<img src="https://www.mistertennis.com/template/ambassador/icon-order.png" width="100%">
					</div>
					<div class="myaccount_menu">
						<h3>Ordini</h3>
						<ul>
							<li><a href="/account/orders/">Visualizza i miei ordini</a></li>
							<li><a href="#">Richiedi un reso</a></li>
							<li><a href="#">Visualizza i miei resi</a></li>	
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="myaccount_section edit_account">
					<div class="myaccount_icon edit_account">
						<img src="https://www.mistertennis.com/template/ambassador/icon-account.png" width="100%">
					</div>
					<div class="myaccount_menu">
						<h3>Modifica Account</h3>
						<ul>
							<li><a href="/account/edit-account/">Modifica i miei dati</a></li>
							<li><a href="/account/edit-address/">I miei indirizzi</a></li>
							<li><a href="/account/edit-account/">Modifica Password</a></li>	
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="myaccount_section customercare">
					<div class="myaccount_icon customercare">
						<img src="https://www.mistertennis.com/template/ambassador/icon-account.png" width="100%">
					</div>
					<div class="myaccount_menu">
						<h3>Customer Care</h3>
						<ul>
							<li><a href="#">Contattaci</a></li>
							<li><a href="#">Cambi taglia, modello e resi</a></li>
							<li><a href="#">Domande Frequenti</a></li>	
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
<?php 
wc_get_template( 'myaccount/template_antony/myaccount_footer.php' );
//sf_woo_help_bar();
//do_action( 'woocommerce_account_navigation' ); ?>

<div class="woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 * @since 2.6.0
		 */
		//do_action( 'woocommerce_account_content' );
	?>
</div>
