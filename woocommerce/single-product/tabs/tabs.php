<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<?php if ( getbowtied_product_layout(get_the_ID()) === 'default' ) : ?>

		<div class="woocommerce-tabs">
			
			<div class="row">
				<div class="large-12 large-centered columns hide">
			
					<ul class="tabs text-center">
						
						<?php foreach ( $tabs as $key => $tab ) : ?>
							<li class="<?php echo esc_attr( $key ); ?>_tab">
						<a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
							</li>
						<?php endforeach; ?>
					</ul>
			
				</div>
			</div>

				<div class="panel entry-content" id="tab-description">
	                <div class="row">
	                    <div class="medium-8 xlarge-5 medium-centered xlarge-centered columns text-center">
	                    	<?php foreach ( $tabs as $key => $tab ) : ?>
	                        <?php call_user_func( $tab['callback'], $key, $tab ) ?>
	                        <?php endforeach; ?>
	                    </div>
	                </div>
	            </div>

			
		</div>


	<?php else: ?>

		<div class="woocommerce-tabs">
			
			<ul class="tabs">
				
				<?php foreach ( $tabs as $key => $tab ) : ?>
					<li class="<?php echo esc_attr( $key ); ?>_tab">
				<a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
			
			<?php foreach ( $tabs as $key => $tab ) : ?>

				<div class="panel entry-content" id="tab-<?php echo esc_attr( $key ); ?>">
                    <?php call_user_func( $tab['callback'], $key, $tab ) ?>
	            </div>

			<?php endforeach; ?>
		</div>


	<?php endif; ?>

<?php endif; ?>