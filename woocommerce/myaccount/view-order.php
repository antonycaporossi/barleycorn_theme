<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*function tracking_myaccount($order){
    $urls = array(
        'BRTCE'         => '<a target="_blank" href="https://vas.brt.it/vas/sped_det_show.hsm?referer=sped_numspe_par.htm&ChiSono=%1$s&ClienteMittente=&DataInizio=&DataFine=&RicercaChiSono=Ricerca">%1$s</a>',
        'DHL'           => '<a target="_blank" href="http://www.dhl.it/it/express/ricerca.html?AWB=%d&brand=DHL">%d</a>',
        'TNTEXPRESS'    =>  '<a target="_blank" href="http://www.tnt.it/tracking/getTrack.html?wt=1&consigNos=%d">%d</a>',
        'FEDEX'         => '<a target="_blank" href="https://www.fedex.com/apps/fedextrack/?action=track&action=track&tracknumbers=%d">%d</a>'
    );
    if($order->get_status() == 'completed'){
        $corriere = $order->get_meta('_order_trackurl');
        $tracking = $order->get_meta('_order_trackno');
        return $corriere.': '.sprintf($urls[$corriere], $tracking);
    }
}*/

?>

<div class="account_header">
	<h1>Ordine # <?php echo $order->get_order_number();?></h1>
	<p>Registrato il <mark><?php echo wc_format_datetime( $order->get_date_created() );?></mark> - <mark><?php echo wc_get_order_status_name( $order->get_status() );?></mark></p>
</div>
<div class="large-5 small-12 large-centered">



<?php if ( $notes = $order->get_customer_order_notes() ) : ?>
	<h2><?php _e( 'Order updates', 'woocommerce' ); ?></h2>
	<ol class="woocommerce-OrderUpdates commentlist notes">
		<?php foreach ( $notes as $note ) : ?>
		<li class="woocommerce-OrderUpdate comment note">
			<div class="woocommerce-OrderUpdate-inner comment_container">
				<div class="woocommerce-OrderUpdate-text comment-text">
					<p class="woocommerce-OrderUpdate-meta meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
					<div class="woocommerce-OrderUpdate-description description">
						<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
					</div>
	  				<div class="clear"></div>
	  			</div>
				<div class="clear"></div>
			</div>
		</li>
		<?php endforeach; ?>
	</ol>
<?php endif; ?>

<?php do_action( 'woocommerce_view_order', $order_id ); ?>
</div>