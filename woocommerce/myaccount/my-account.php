<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

/**
 * My Account navigation.
 * @since 2.6.0
 */
?>
<div class="account_header">
	<h1>Il tuo account</h1>
	<p>Gestisci qui le tue impostazioni e le tue preferenze</p>
</div>
<div class="large-8 large-centered">
<?php
//do_action( 'woocommerce_account_navigation' ); ?>

<div class="woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 * @since 2.6.0
		 */
		//do_action( 'woocommerce_account_content' );

	?>
	<div class="myaccount_welcomeback">
		<h2>Bentornato, <span><?php echo esc_html( $current_user->display_name );?></span></h2>
		<div class="myaccount_actions">
		<a href="<?php echo get_site_url(); ?>/?<?php echo get_option('woocommerce_logout_endpoint'); ?>=true" class="logout_link">LOGOUT</a>&nbsp;&nbsp;
		<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account' ) );?>">MODIFICA PROFILO</a>
	</div>
		<div class="clear"></div>
	</div>
	<div class="myaccount_orders">
		<h2>I tuoi ordini</h2>
		<?php woocommerce_account_orders(-1) ?>
	</div>
	<div class="myaccount_address">
		<h2>I tuoi indirizzi</h2>
		<?php wc_get_template( 'myaccount/my-address.php' ); ?>
	</div>
</div>
</div>