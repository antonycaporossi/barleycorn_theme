<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_orders', $has_orders ); ?>

<?php if ( $has_orders ) : ?>
	<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
		<?php foreach ( $customer_orders->orders as $k=>$customer_order ) :
			$order      = wc_get_order( $customer_order );
			$item_count = $order->get_item_count();
			$active = '';
			if($k == 0){
				$active = 'is-active';
			}
			?>
		<li class="accordion-item <?php echo $active;?>" data-accordion-item>
			<div class="accordion-header accordion-title">
				<div class="accordion-header-p">
					<a href="#"><?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?></a>
					<p class="accordion-header-item-amount"><?php printf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count );?></p>
				</div>
				<div class="accordion-header-p">
					<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>
					<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>
				</div>
			</div>
			<div class="accordion-content" data-tab-content>
				<div class="order-items-container">
					<?php
						foreach ( $order->get_items() as $item_id => $item ) {
							$image_src = wc_placeholder_img_src();
							$product = wc_get_product($item->get_product_id());
							if ($product->get_image_id()){
								$image_src = current( wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail' ) );
							}
					?>
						<div class="order-item">
							<div class="order-item-info">
								<div class="order-item-image">
									<img src="<?php echo $image_src; ?>" width="134px"/>
								</div>
								<div class="order-item-data">
									<p class="order-item-title"><?php echo $product->get_title(); ?></p>
									<p class="order-item-attributes-quantity">Quantità: x<?php echo $item->get_quantity();?></p>
								</div>
							</div>
							<div class="order-item-price">
								<?php echo wc_price($item->get_total());?>
							</div>
						</div>
					<?php
						}
					?>
				</div>
				<div class="order-totals">
					<table class="order-totals">
						<tr>
							<td>Subtotale:</td>
							<td><?php echo wc_price($order->get_subtotal());?></td>
						</tr>
						<?php
							if($order->get_coupon_codes()):
						?>
						<tr>
							<td>Codice sconto:</td>
							<td><?php echo $order->get_coupon_codes()[0]; ?></td>
						</tr>
						<?php
							endif;	
						?>
						<?php
							if($order->get_total_discount()):
						?>
						<tr>
							<td>Sconto:</td>
							<td><?php echo wc_price($order->get_total_discount()) ; ?></td>
						</tr>
						<?php
							endif;	
						?>
						<tr>
							<td>Totale:</td>
							<td><?php echo wc_price($order->get_total());?></td>
						</tr>
					</table>
				</div>
				<div class="order-action">

				</div>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
	<table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead>
			<tr>
				<?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
					<th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php foreach ( $customer_orders->orders as $customer_order ) :
				$order      = wc_get_order( $customer_order );
				$item_count = $order->get_item_count();
				?>
				<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order">
					<?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
						<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
							<?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
								<?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

							<?php elseif ( 'order-number' === $column_id ) : ?>
								<a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
									<?php echo _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number(); ?>
								</a>

							<?php elseif ( 'order-date' === $column_id ) : ?>
								<time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

							<?php elseif ( 'order-status' === $column_id ) : ?>
								<?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

							<?php elseif ( 'order-total' === $column_id ) : ?>
								<?php
								/* translators: 1: formatted order total 2: total order items */
								printf( _n( '%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count );
								?>

							<?php elseif ( 'order-actions' === $column_id ) : ?>
								<?php
								$actions = wc_get_account_orders_actions( $order );

								if ( ! empty( $actions ) ) {
									foreach ( $actions as $key => $action ) {
										echo '<a href="' . esc_url( $action['url'] ) . '" class="woocommerce-button button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
									}
								}
								?>
							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

	<?php if ( 1 < $customer_orders->max_num_pages ) : ?>
		<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
			<?php if ( 1 !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) ); ?>"><?php _e( 'Previous', 'woocommerce' ); ?></a>
			<?php endif; ?>

			<?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) ); ?>"><?php _e( 'Next', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>

<?php else : ?>
	<div>
		<?php wpautop(_e( 'No order has been made yet.', 'woocommerce' )); ?><br/>
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php _e( 'Go shop', 'woocommerce' ) ?>
		</a>
	</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>
