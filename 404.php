<?php get_header(); ?>

	<div id="primary" class="content-area">

        <div class="row">	
            <div class="large-12 small-12 large-centered columns">    
                <div id="content" class="site-content" role="main">
                
                    <section class="error-404 not-found">
                        <h2 class="subhead neuefont">Errore 404</h2>
                        <div class="mbox-wrapper">
                            <div class="mbox-media"></div>
                            <div class="mbox neuefont">OOPS<span>!</span></div>
                        </div>
                        <h3 class="title">Questa pagina non esiste più.</h3>
                        <p class="copy neuefont">In non tincidunt mi. Etiam id malesuada erat.<br/>Aliquam erat volutpat. Maecenas at posuere tellus, sit amet iaculis ipsum.</p>

                    </section><!-- .error-404 -->
                    
                </div><!-- #content -->
            </div><!-- .large-12 .columns -->                
        </div><!-- .row -->
        <div class="mayinterested">
        <h2 class="text-center">Ricomincia il tuo shopping</h2>
    <div style="position:relative">
    <?php echo do_shortcode('[product_category per_page="12" columns="8" orderby="" order="" category="uomo"]'); ?>
</div>
</div>

    </div><!-- #primary -->

<?php get_footer(); ?>